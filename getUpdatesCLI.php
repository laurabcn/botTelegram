#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

$bot_api_key  = '586638028:AAFQwVLCatzEoMn_NcFGvhTOJAy6dJ0D2lc';
$bot_username = 'rajoySaysBot';

$mysql_credentials = [
    'host'     => 'localhost',
    'user'     => 'root',
    'password' => '',
    'database' => 'bot',
];

try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);

    // Enable MySQL
    $telegram->enableMySql($mysql_credentials);

    $telegram->enableLimiter();

    // Handle telegram getUpdates request
    $telegram->handleGetUpdates();
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // log telegram errors
    // echo $e->getMessage();
}