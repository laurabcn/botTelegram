<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\PhotoSize;
use Longman\TelegramBot\Request;

/**
 * User "/survey" command
 *
 * Command that demonstrated the Conversation funtionality in form of a simple survey.
 */
class VoyageCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'voyage';

    /**
     * @var string
     */
    protected $description = 'Survery for bot users';

    /**
     * @var string
     */
    protected $usage = '/voyage';

    /**
     * @var string
     */
    protected $version = '0.3.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * @var bool
     */
    protected $private_only = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

    private $url = 'http://transport.local:8080/app_dev.php/api/search';

    private $typeSearch = ['Ida', 'Ida/Vuelta', 'Multidestino'];

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();

        //Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];



        if ($chat->isGroupChat() || $chat->isSuperGroup()) {
            //reply to message id is applied by default
            //Force reply is applied by default so it can work with privacy on
            $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
        }

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

      // $this->conversation->stop();
        //$result = Request::emptyResponse();

        //State machine
        //Entrypoint of the machine state if given by the track
        //Every time a step is achieved the track is updated
        switch ($state) {
            case 0:

                if ($text === '') {
                    $notes['state'] = 0;
                    $this->conversation->update();

                    $data['text']         = 'Tipo de viaje:';
                    $data['reply_markup'] = (new Keyboard([
                        $this->typeSearch[0],
                        $this->typeSearch[1],
                        $this->typeSearch[2],
                    ]))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                if (in_array($text, $this->typeSearch)) {
                    $notes['typeSearch'] = $text;
                    $text = '';
                }else{
                    $text = '';
                    $result = Request::sendMessage($data);
                    break;
                }

            // no break
            case 1:
                if ($text === '') {
                    $notes['state'] = 1;
                    $this->conversation->update();

                    $data['text'] = 'De donde sales?';

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['origen'] = $text;
                $text             = '';

            // no break
            case 2:
                if ($text === '') {
                    $notes['state'] = 2;
                    $this->conversation->update();

                    $data['text'] = 'A donde vas?';

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['destination'] = $text;
                $text         = '';

            case 3:
                if ($text === '') {
                    $notes['state'] = 3;
                    $this->conversation->update();

                    $data['text'] = 'Cuando sales?';
                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['fecha_salida'] = $text;
                $text         = '';
            // no break



            case 4:
                if ($text === '') {
                    $notes['state'] = 4;
                    $this->conversation->update();

                    $data['text'] = 'Cuando llegas?';

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['fecha_regreso'] = $text;
                $text         = '';
            // no break
            case 5:
                if ($text === ''||!is_numeric($text)) {
                    $notes['state'] = 5;
                    $this->conversation->update();

                    $data['text'] = 'Cuantos pasajeros?';

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['adultos'] = $text;
                $text         = '';



            $journeys = $notes['origen'].','.$notes['destination'].'|'.$notes['destination'].','.$notes['origen'];

            $params = ['country_context'=>'ES',
                'agent'=>'ATRAPALO',
                'channel'=>'COMMON',
                'journeys'=>$journeys,
                'dates'=>'20180821|20180910',
                'adults'=>1,
                'children'=>0,
                'babies'=>0,
                'search_strategy'=>'queue'
            ];
            $params_json = json_encode($params);

            $ch = curl_init($this->url);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params_json))
            );

            $response = curl_exec($ch);

            curl_close($ch);

            $data['text'] = $response;
            $result = Request::sendMessage($data);
            break;

            case 6:
                $this->conversation->update();
                $out_text = '/Atrapalo result:' . PHP_EOL;
                unset($notes['state']);
                foreach ($notes as $k => $v) {
                    $out_text .= PHP_EOL . ucfirst($k) . ': ' . $v;
                }

                $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                $data['text']      = $out_text;
                $this->conversation->stop();

                $result = Request::sendMessage($data);
                break;

        }

        return $result;
    }
}
