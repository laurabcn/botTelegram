<?php

//TODO: ejecutar php en servidor php -S 127.0.0.1:8040 hook.php
//TODO: ngrok http 8040 para abrir socket https
//TODO: sethooks https://api.telegram.org/bot586638028:AAFQwVLCatzEoMn_NcFGvhTOJAy6dJ0D2lc/setWebhook?url=https://da264744.ngrok.io/hook.php

// Load composer
require __DIR__ . '/vendor/autoload.php';

$bot_api_key  = '586638028:AAFQwVLCatzEoMn_NcFGvhTOJAy6dJ0D2lc';
$bot_username = 'rajoySaysBot';

$dir = __DIR__;


$commands_paths = [
    $dir . '/Commands/',
];

$mysql_credentials = [
    'host'     => '127.0.0.1',
    'user'     => 'root',
    'password' => '',
    'database' => 'bot',
];

try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);

    $telegram->addCommandsPaths($commands_paths);

    $telegram->setUploadPath($dir.'/images/');

    // Enable MySQL
    $telegram->enableMySql($mysql_credentials);

    // Handle telegram webhook request
    $telegram->handle();
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // Silence is golden!
    // log telegram errors
    // echo $e->getMessage();
}